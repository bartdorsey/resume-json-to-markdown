// Define the resume module
define(['jquery','handlebars'],function($,Handlebars){
    var ResumeComponent = function () {
      var self = this;
    };

    ResumeComponent.prototype.getTemplate = function (name,onComplete) {
      $.get('templates/' + name + '.md',function(template){
        onComplete(template);
      });
    };

    ResumeComponent.prototype.renderTemplate = function(name,object,onComplete) {
      this.getTemplate(name,function(template){
        var h = Handlebars.compile(template);
        onComplete(h(object));
      });
    };

    return ResumeComponent;
  }
);
